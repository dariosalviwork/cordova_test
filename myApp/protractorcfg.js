exports.config = {

  seleniumAddress: 'http://localhost:4723/wd/hub',

  specs: ['./tests/*.js'],

  capabilities: {
    platformName: 'android',
    platformVersion: '5.1.1',
    deviceName: 'test',
    browserName: '',
    autoWebview: true,
    appActivity: 'MainActivity',
    appPackage: 'com.ionicframework.starter'
  },

  baseUrl: 'http://10.0.2.2:8000',

  onPrepare: function(){
    var wd = require('wd');
    var protractor = require('protractor');
    var wdBridge = require('wd-bridge')(protractor, wd);
    wdBridge.initFromProtractor(exports.config)
  }
}

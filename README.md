Automatic tests with cordova projects in emulator (or actual device)
====================================================================

I assume you have android SDK installed, JDK and a virtual device set up.

if you need to create a device:
android create avd -n testdevice -t android-22


Plain cordova project
---------------------

install cordova-test globally: (add sudo before each line if you're on Mac or linux)
```bash
npm -g install mocha
npm -g install cordova-test
```

then create a cordova project: (already provided in this repo!)
```bash
cordova create example com.example.helloworld
```

go to the project's folder and build everything:
```bash
cd example
cordova platform add android
cordova build android
```

if you want to see if it works on your machine:
```bash
cordova emulate android
```

now add some tests, this repo already comes with one in example/tests

if you want to use the same kind of environment you need to also install:
```bash
npm install chai-as-promised minimist wd
```

start the tests with appium and cordova-test:
```bash
appium &
cordova-test android tests/test.js
```


Ionic (or any other angular 1 project)
--------------------------------------

we can use ionic as an angular 1 framework:
```bash
npm install -g ionic
```

create a project (the project is already created here)
```bash
ionic start myApp tabs
```

compile the project for a platform (we'll use Android here)
```bash
cd myApp
cordova platform add android
cordova build android
```

(optional) check that it works locally on your computer:
```bash
cordova emulate android
```

install appium:
```bash
npm install -g appium
```

install and run appium doctor to check if everything is well configured
```bash
npm -g appium-doctor
appium-doctor
```

basically you need to have the JDK and the Android SDK installed and proper JAVA_HOME and ANDROID_HOME environment variables well set

as testing framework we are going to use protractor (the one recommended by Google for e2e tests with Angular)
```bash
npm install -g protractor
```

then we need to add the protractor configuration, see protractorcfg.js in this repo

an explanation of the capabilities is offered [here](http://appium.io/slate/en/master/?ruby#appium-server-capabilities)

you can also see that it uses some modules (wd and wd-bridge) that we need to install locally
```bash
npm install wd wd-bridge protractor --savedev
```

you can just use the package.json of the example under myApp and do
```bash
npm install
```

we put the tests inside a folder called tests (see the configuration) using protractor style

start appium
```bash
appium &
```

start an emulator or connect an actual phone through USB

run the tests
```bash
protractor protractorcfg.js
```
